## Description

Java8のStreamAPIをコードを書いて試してみます.

## Requirement

* [JDK8]
* [NetBeans 8.0.2] (want)

## References

* [Java Streamメモ(Hishidama's Java8 Stream Memo)]
* [Java8のStreamを使いこなす(きしだのはてな)]

## Author

[Yusuke Kawatsu]



[Yusuke Kawatsu]:https://bitbucket.org/megmogmog1965/
[NetBeans 8.0.2]:https://ja.netbeans.org/
[JDK8]:http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Java Streamメモ(Hishidama's Java8 Stream Memo)]:http://www.ne.jp/asahi/hishidama/home/tech/java/stream.html#Stream.collect
[Java8のStreamを使いこなす(きしだのはてな)]:http://d.hatena.ne.jp/nowokay/20130504
