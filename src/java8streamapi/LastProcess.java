/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8streamapi;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author meg.mog.mog1965
 */
public class LastProcess {
    
    /**
     * Steamの要素がすべて、指定した条件(lambda)を満たすかどうか.
     */
    public void allMatch() {
        boolean result = Stream.of("hello Taro", "hello Hanako", "hello John")
                .allMatch(s -> s.startsWith("hello"));
        
        System.out.println("result: " + result);
    }

    /**
     * Steamの要素の何れかが、指定した条件(lambda)を満たすかどうか.
     */
    public void anyMatch() {
        boolean result = Stream.of("hello Taro", "hello Hanako", "hello John")
                .anyMatch(s -> s.contains("Hanako"));
        
        System.out.println("result: " + result);
    }

    public void average() {
        double average = IntStream.of(50, 65, 42, 80, 100, 77)
                .average()
                .getAsDouble();
        
        System.out.println("Average: " + average);
    }

    /**
     * Collectorクラスを渡して、結果を受け取る.
     * 
     * @note Collectorクラスは大量にあるので、詳しくは java.util.stream.Collectors を見ること.
     */
    public void collect() {
        // 平均を出してみる...
        double average = Stream.of(50, 65, 42, 80, 100, 77)
                .collect(Collectors.averagingDouble(d -> (double)d));
        
        System.out.println("Average: " + average);
    }

    /**
     * Streamの要素の数を返す.
     */
    public void count() {
        long count = Stream.of(1, 2, 3, 4)
                .count();
        
        System.out.println("Count: " + count);
    }

    /**
     * Steamの何れかの要素を返す.
     */
    public void findAny() {
        String name = Stream.of("Taro", "Hanako", "John")
                .findAny()
                .get();
        
        System.out.println("Name: " + name);
    }

    /**
     * * Steamの最初の要素を返す.
     */
    public void findFirst() {
        String name = Stream.of("Taro", "Hanako", "John")
                .findFirst()
                .get();
        
        System.out.println("Name: " + name);
    }

    /**
     * 値を処理する関数(lambda)を渡し、全ての要素に副作用を起こす.
     * 
     * @note 副作用なので戻り値はない.
     */
    public void forEach() {
        Stream.of("Taro", "Hanako", "John")
                .forEach(System.out::println);
    }

    /**
     * 値を処理する関数(lambda)を渡し、全ての要素に副作用を起こす.
     * 
     * @note parallel()でも、順番が守られる.
     */
    public void forEachOrdered() {
        Stream.of("Taro", "Hanako", "John")
                .parallel()
                .forEachOrdered(System.out::println);
    }

    /**
     * Iteratorを返す.
     */
    public void iterator() {
        Iterator<String> iter = Stream.of("Taro", "Hanako", "John")
                .iterator();
        
        for( ; iter.hasNext() ; ) {
            System.out.println(iter.next());
        }
    }

    /**
     * 最大値を返す.
     */
    public void max() {
        int max = IntStream.of(1, 2, 3, 4)
                .max()
                .getAsInt();
        
        System.out.println("Max: " + max);
    }

    /**
     * 最小値を返す.
     */
    public void min() {
        int min = IntStream.of(1, 2, 3, 4)
                .min()
                .getAsInt();
        
        System.out.println("Min: " + min);
    }

    /**
     * Steamの要素の全てが、指定した条件(lambda)を満たさない、かどうか.
     */
    public void noneMatch() {
        boolean result = Stream.of("hello Taro", "hello Hanako", "hello John")
                .noneMatch(s -> s.startsWith("goodbye"));
        
        System.out.println("result: " + result);
    }

    /**
     * 値を一つにまとめる.
     * 
     * @note Collectorが使える時は、そっちを使ったほうが簡潔.
     */
    public void reduce() {
        int sum = IntStream.of(1, 2, 3, 4)
                .reduce(0, (s, d) -> s + d);
        
        System.out.println("Sum: " + sum);
    }

    /**
     * 合計値を返す.
     */
    public void sum() {
        int sum = IntStream.of(1, 2, 3, 4)
                .sum();
        
        System.out.println("Sum: " + sum);
    }

    /**
     * 平均、要素数、最大値、最小値、合計を返す.
     */
    public void summaryStatistics() {
        IntSummaryStatistics summary = IntStream.of(1, 2, 3, 4)
                .summaryStatistics();
        
        System.out.println("Average: " + summary.getAverage());
        System.out.println("Count: " + summary.getCount());
        System.out.println("Max: " + summary.getMax());
        System.out.println("Min: " + summary.getMax());
        System.out.println("Sum: " + summary.getSum());
    }

    /**
     * 配列にして返す.
     */
    public void toArray() {
        Object[] objectList = Stream.of("Taro", "Hanako", "John")
                .toArray();
        
        for(int i=0 ; i < objectList.length ; i++) {
            System.out.println(objectList[i]);
        }
    }

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object object = new LastProcess();

        try {
            // collect all methods.
            // @see http://stackoverflow.com/questions/5266532/can-i-get-all-methods-of-a-class
            Class c = LastProcess.class;
            Method[] methods = c.getDeclaredMethods();

            // invoke all.
            Arrays.asList(methods).stream()
                    .filter(m -> m.getParameterCount() == 0)
                    .forEach(m -> {
                        // チェック例外がlambdaで悲劇を生む...
                        // @see http://stackoverflow.com/questions/19757300/java-8-lambda-streams-filter-by-method-with-exception
                        try {
                            System.out.println("\n==== " + m.toString() + " ====================\n");
                            m.invoke(object, null);
                        } catch (Throwable e) {
                            System.err.println(e);
                        }
                    });

        } catch (Throwable e) {
            System.err.println(e);
        }
    }

}
