/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8streamapi;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

/**
 *
 * @author meg.mog.mog1965
 */
public class StreamFactory {

    /**
     * StreamのBuilderを返す.
     */
    public void builder() {
        Builder<String> builder = Stream.builder();
        Stream<String> stream = builder.add("foo").add("bar").add("hoge").add("fuga").build();

        stream.forEach(System.out::println);
    }

    /**
     * 2個のStreamをつなげて、1個のStreamを作る.
     */
    public void concat() {
        Stream<String> streamA = Stream.of("aaa", "bbb", "ccc");
        Stream<String> streamB = Stream.of("111", "222", "333");
        
        Stream.concat(streamA, streamB)
                .forEach(System.out::println);
    }

    /**
     * 空のStreamを作る.
     */
    public void empty() {
        Stream.empty()
                .forEach(System.out::println);
    }

    /**
     * 値を返す関数を指定してStreamを作る.
     * 
     * @note 無限に続くので、limitと組み合わせる.
     */
    public void generate() {
        Stream.generate(() -> "hello")
                .limit(5)
                .forEach(System.out::println);
    }

    /**
     * 前の値を使って、次の値を生成する.
     * 
     * @note 無限に続くので、limitと組み合わせる.
     */
    public void iterate() {
        Stream.iterate("a", c -> c + c)
                .limit(5)
                .forEach(System.out::println);
    }

    /**
     * 値や配列から直接Streamを作る.
     */
    public void of() {
        Stream.of("aaa", "bbb", "ccc")
                .forEach(System.out::println);
    }

    /**
     * 整数の開始/終了の範囲でStreamを作る.
     */
    public void range() {
        IntStream.range(1, 5)
                .forEach(System.out::println);
    }

    /**
     * 整数の開始/終了の範囲でStreamを作る.
     * 
     * @note 終端の数字を含む
     */
    public void rangeClosed() {
        IntStream.rangeClosed(1, 5)
                .forEach(System.out::println);
    }

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object object = new StreamFactory();

        try {
            // collect all methods.
            // @see http://stackoverflow.com/questions/5266532/can-i-get-all-methods-of-a-class
            Class c = StreamFactory.class;
            Method[] methods = c.getDeclaredMethods();

            // invoke all.
            Arrays.asList(methods).stream()
                    .filter(m -> m.getParameterCount() == 0)
                    .forEach(m -> {
                        // チェック例外がlambdaで悲劇を生む...
                        // @see http://stackoverflow.com/questions/19757300/java-8-lambda-streams-filter-by-method-with-exception
                        try {
                            System.out.println("\n==== " + m.toString() + " ====================\n");
                            m.invoke(object, null);
                        } catch (Throwable e) {
                            System.err.println(e);
                        }
                    });

        } catch (Throwable e) {
            System.err.println(e);
        }
    }

}
