/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java8streamapi;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 *
 * @author meg.mog.mog1965
 */
public class MidProcess {
    
    /**
     * DoubleStreamを返す.
     */
    public void asDoubleStream() {
        DoubleStream stream = IntStream.of(1, 2, 999, 77, 5)
                .asDoubleStream();

        stream.forEach(System.out::println);
    }

    /**
     * LongStreamを返す.
     */
    public void asLongStream() {
        LongStream stream = IntStream.of(1, 2, 999, 77, 5)
                .asLongStream();

        stream.forEach(System.out::println);
    }

    /**
     * Stream&lt;XXX&gt; (ラッパークラス) にして返す.
     */
    public void boxed() {
        Stream<Integer> stream = IntStream.of(1, 2, 999, 77, 5)
                .boxed();

        stream.forEach(System.out::println);
    }

    /**
     * 重複した値を除いたStream.
     */
    public void distinct() {
        Stream.of("hello", "goodbye", "hello")
                .distinct()
                .forEach(System.out::println);
    }

    /**
     * 指定した条件 (lambda) に合致するものだけ残したStreamを返す.
     */
    public void filter() {
        Stream.of("hello taro", "goodbye hanako", "hello john")
                .filter(s -> s.startsWith("hello"))
                .forEach(System.out::println);
    }

    /**
     * 複数のStreamを一つのStreamにして (flatにして) 返す.
     */
    public void flatMap() {
        Stream.of("1 2", "3", "4 5 6")
                .flatMap(s -> Stream.of(s.split(" ")))
                .forEach(System.out::println);
    }

    /**
     * 複数のStreamを一つのDoubleStreamにして (flatにして) 返す.
     */
    public void flatMapToDouble() {
        Stream.of(1, 2, 3, 4)
                .flatMapToDouble(d -> DoubleStream.of(d, d + 0.5f))
                .forEach(System.out::println);
    }

    /**
     * 複数のStreamを一つのIntStreamにして (flatにして) 返す.
     */
    public void flatMapToInt() {
        Stream.of(1, 2, 3, 4)
                .flatMapToInt(d -> IntStream.of(d, d * 10))
                .forEach(System.out::println);
    }

    /**
     * 複数のStreamを一つのLongStreamにして (flatにして) 返す.
     */
    public void flatMapToLong() {
        Stream.of(1, 2, 3, 4)
                .flatMapToLong(d -> LongStream.of(d, d * 10))
                .forEach(System.out::println);
    }

    /**
     * Stream中の個数を限定して返す.
     */
    public void limit() {
        Stream.of("one", "two", "three", "four", "five")
                .limit(3)
                .forEach(System.out::println);
    }

    /**
     * 値を変換したStreamを返す.
     * 
     * @note 引数と戻り値の型は同じ.
     */
    public void map() {
        Stream.of("one", "two", "three", "four", "five")
                .map(s -> s.toUpperCase())
                .forEach(System.out::println);
    }

    /**
     * (引数に関わらず) 値を変換したDoubleStreamを返す.
     */
    public void mapToDouble() {
        Stream.of("one", "two", "three", "four", "five")
                .mapToDouble(s -> s.length())
                .forEach(System.out::println);
    }

    /**
     * (引数に関わらず) 値を変換したIntStreamを返す.
     */
    public void mapToInt() {
        Stream.of("one", "two", "three", "four", "five")
                .mapToInt(s -> s.length())
                .forEach(System.out::println);
    }

    /**
     * (引数に関わらず) 値を変換したLongStreamを返す.
     */
    public void mapToLong() {
        Stream.of("one", "two", "three", "four", "five")
                .mapToLong(s -> s.length())
                .forEach(System.out::println);
    }

    /**
     * Primitive値を変換したStream&lt;Object&gt;を返す.
     */
    public void mapToObj() {
        IntStream.of(1, 2, 3, 4)
                .mapToObj(d -> "..." + d)
                .forEach(System.out::println);
    }

    /**
     * StreamのClose時 ( close()が呼ばれた時 == Streamの破棄の時 ? ) に呼ばれるハンドラを登録する.
     */
    public void onClose() {
        Stream.of(1, 2, 3, 4)
                .onClose(() -> System.out.println("Stream closed !"))
                .forEach(System.out::println);
    }

    /**
     * 並列Stream (Multithreadで処理) を返す.
     */
    public void parallel() {
        Stream.of(1, 2, 3, 4)
                .parallel()
                .forEach(System.out::println);
    }

    /**
     * 値を受け取る関数（Consumer）を渡す.
     * 
     * @note 保持している値を関数に渡すが、出力Streamとしては何も変わらない。デバッグ用途.
     */
    public void peek() {
        Stream.of(1, 2, 3, 4)
                .peek(System.out::println)
                .forEach(System.out::println);
    }

    /**
     * 直列Stream (foreachと同じ、いつものやつ) を返す.
     */
    public void sequential() {
        List<Integer> intList = Arrays.asList(1, 2, 3, 4);
        
        intList.parallelStream()    // parallelを...
                .sequential()       // sequentialに戻す、無意味なコード
                .forEach(System.out::println);
    }

    /**
     * 指定した個数をSkip(無視)したStreamを返す.
     */
    public void skip() {
        Stream.of(1, 2, 3, 4)
                .skip(2)
                .forEach(System.out::println);
    }

    /**
     * ソート済みのStreamを返す.
     * 
     * @note 比較関数を渡すことも可能.
     */
    public void sorted() {
        Stream.of(1, 2, 999, 88, 5)
                .sorted()
                .forEach(System.out::println);
    }

    /**
     * 順序を持たないStreamを返す.
     */
    public void unordered() {
        Stream.of(1, 2, 999, 88, 5)
                .unordered()
                .forEach(System.out::println);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object object = new MidProcess();

        try {
            // collect all methods.
            // @see http://stackoverflow.com/questions/5266532/can-i-get-all-methods-of-a-class
            Class c = MidProcess.class;
            Method[] methods = c.getDeclaredMethods();

            // invoke all.
            Arrays.asList(methods).stream()
                    .filter(m -> m.getParameterCount() == 0)
                    .forEach(m -> {
                        // チェック例外がlambdaで悲劇を生む...
                        // @see http://stackoverflow.com/questions/19757300/java-8-lambda-streams-filter-by-method-with-exception
                        try {
                            System.out.println("\n==== " + m.toString() + " ====================\n");
                            m.invoke(object, null);
                        } catch (Throwable e) {
                            System.err.println(e);
                        }
                    });

        } catch (Throwable e) {
            System.err.println(e);
        }
    }

}
